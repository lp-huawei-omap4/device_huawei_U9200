# boot animation
TARGET_SCREEN_HEIGHT := 960
TARGET_SCREEN_WIDTH := 540
TARGET_CONTINUOUS_SPLASH_ENABLED := true
TARGET_BOOTANIMATION_HALF_RES := true

# Inherit some common CM stuff.
$(call inherit-product, device/huawei/viva/device.mk)
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := viva
PRODUCT_NAME := cm_viva
PRODUCT_RELEASE_NAME := U9200
PRODUCT_BRAND := Huawei
PRODUCT_MODEL := U9200
PRODUCT_MANUFACTURER := HUAWEI
PRODUCT_GMS_CLIENTID_BASE := android-huawei

ifneq ($(CM_BUILDTYPE),UNOFFICIAL)
    CM_BUILDTYPE := ShevT
    CM_VERSION := $(PRODUCT_VERSION_MAJOR)-$(shell date -u +%Y%m%d)-$(CM_BUILDTYPE)-$(CM_BUILD)$(CM_EXTRAVERSION)
endif

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=U9200 \
    TARGET_DEVICE=viva \
    BUILD_FINGERPRINT="Huawei/U9200/hwu9200:4.1.1/HuaweiU9200/CHNC00B710:user/release-keys" \
    PRIVATE_BUILD_DESC="front-user 4.1.1 JRO03L 99 test-keys"
